const mobileMenu = document.querySelector('.mobile-menu');
const mobileUl = document.querySelector('.mobile-nav-ul');

mobileMenu.addEventListener('click', e => {
    mobileMenu.classList.toggle('active');
    mobileUl.classList.toggle('active');
});

window.addEventListener('click', e => {
    if (!(e.target.classList.contains('mobile-menu') || e.target.classList.contains('line'))) {
        mobileUl.classList.remove('active');
        mobileMenu.classList.remove('active');
    }
});
